﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace WheR.Hubs
{
	public class WheRHub : Hub
	{
		public void BroadcastLocationToAll(string lat, string lng)
		{
			Clients.All.addPin(lat, lng);
		}
	}
}